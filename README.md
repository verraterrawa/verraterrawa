VerraTerra Real Estate and Property Management Services

VerraTerra was founded in 2009 to bring a fair way for consumers to sell or rent their home without spending unfair rent based or commissions based on sale prices.

Address: 16212 Bothell-Everett Hwy, #F319, Mill Creek, WA 98012, USA

Phone: 206-408-8077

Website: http://verraterra.com
